# given: string in json format but without required quotes

# goal: parsed json

# isinstance(<var>, int)

# states:
#   key (after ", ")
#   value (after ": ")

def parse(j):
    key = False
    value = False
    array = False
    skip = False
    curKey = ""
    curVal = ""
    # >>> s[:4] + '-' + s[4:]
    if(j[0] == "["):
        arr = list()
        array = True

    cur = dict()

    for x in range(len(j)):
        if(skip):
            skip = False
            continue
        if(j[x] == "{"):
            key = True
        elif(j[x] == ":" and key):
            value = True
            key = False
            skip = True #skip whitespace
        elif(j[x] == "}" and value):
            curKey = curKey
            curVal = curVal
            cur[curKey] = curVal
            curKey = ""
            curVal = ""
            value = False
            if(array):
                arr.append(cur)
                cur = dict()
        elif(j[x] == "," and value):
            if(lookAhead(j, x) == "}"): #value contains comma (-> this comma is not a separator)
                b = lookAheadPos(j, x)
                a = x
                curVal = curVal + j[a: b]
                cur[curKey] = curVal
                curKey = ""
                curVal = ""
                value = False
                if(array):
                    arr.append(cur)
                    cur = dict()
            else:
                cur[curKey] = curVal
                curKey = ""
                curVal = ""
                value = False
                key = True
                skip = True
        elif(key):
            curKey = curKey + j[x]
        elif(value):
            curVal = curVal + j[x]
        # elif(not key and not value):
        #  -> in this case we just skip, it will get naturally handled

    if(array):
        return arr
    else:
        return cur

def parseTimestamp(t):
    value = False
    key = ""
    skip = False
    val = ""
    arr = list()
    out = dict()
    for x in range(len(t)):
        if skip:
            skip = False
            continue
        elif(t[x] == "["): #  new sub-array begins, start reading values
            value = True
        elif(t[x] == "]"): #  current sub-array ended: append value to sub-array, sub-array to out array
            out[key] = val
            arr = list()
            value = False
            val = ""
            key = ""
        elif(t[x] == "," and value): #current value ended: add to current sub-array and delete
            key = val
            val = ""
            skip = True
        elif(value): #another char of the current value
            val = val + t[x]
        #print(t[x])
        #print(val)
        #print(arr)
        #print(out)
    return out

def lookAhead(string, position):
    while(string[position] not in {":", "}"}):
        position += 1
    return string[position]

def lookAheadPos(string, position):
    while(string[position] not in {":", "}"}):
        position += 1
    return position

