import os
import json
import math
import pandas as pd
import ast
import numpy as np
import json
#custom parsers:
import parseJson as j
from datetime import datetime

def write_emails_to_json(email_list, file_path):
    try:
        with open(file_path, 'w') as file:
            json.dump(email_list, file, indent=2)
        print(f"Emails successfully written to {file_path}")
    except IOError as e:
        print(f"Error writing to file: {e}")

def baseprofile(array):
    """
    :param array:
    :return: True if it is a base profile else False
    """
    notzero = False
    for x in array:
        if(x != 0):
            if(not notzero):
                notzero = True
            else:
                return False
    return True



#changes:
# fixed:
# ke5av04aifh: twenty four -> 24
# 78qwfsvwxhi: 20years -> 20
# exclude:
# age is a: 765sa3tkwyk, 8s9wvzfnahc, gcp7l7pmh5
# 1h9qi7tnl93: said 152
# 38d7gwav007 said kdkd
def parseAge(x) :
    x = x.strip()
    x = x.split(' ')[0]
    try :
        x = int(x)
        if x < 90 and x >= 14: #age constrained to be at least 14 and less than 90
            return x
        return np.nan
    except :
        return np.nan

#path to root directory containing all the results sub folders
root = '/Users/Tobias/PycharmProjects/BachelorThesis/study_results/'
conversationPath = '/Users/Tobias/PycharmProjects/BachelorThesis/user_conversations'

def datedif_seconds(date_str1, date_str2):
    format_str = "%Y-%m-%d %H:%M:%S.%f"

    # Convert strings to datetime objects
    date_time1 = datetime.strptime(date_str1, format_str)
    date_time2 = datetime.strptime(date_str2, format_str)

    # Calculate the time difference
    time_difference = date_time2 - date_time1

    return time_difference.total_seconds()

def datedif(date_str1, date_str2):
    format_str = "%Y-%m-%d %H:%M:%S.%f"

    # Convert strings to datetime objects
    date_time1 = datetime.strptime(date_str1, format_str)
    date_time2 = datetime.strptime(date_str2, format_str)

    # Calculate the time difference
    time_difference = date_time2 - date_time1

    return time_difference

# dataframe columns:
#  -> the following lists representing dataframe columns define the dataframes, the rows will not be labeled
# there are three data frames, the chat df, the user df and the rating df
# user df:
pre_columns = ["user_id", "age", "gender", "enrolled", "university", "degree", "english_level", "chatbot_experience", "pre_timestamps"]
post_columns = ["user_id", "chatbot_authenticity", "chatbot_sophisticated", "trait_remove", "trait_add", "truthful", "mail", "remarks", "post_timestamps"]


user_columns = pre_columns + post_columns[1:] + ["ratings_amount"]


personality = ["actual_vibrancy", "actual_conscientiousness", "actual_civility", "actual_artificiality", "actual_neuroticism"]
meta_ratings = ["enjoyable", "flow", "consistent", "reasonable_personality", "trust", "trust important"]

# chats df: (intermediate
chats_columns = ["chat_id"] + personality + meta_ratings + ["model", "persona", "topic", "convo_timestamps"]

# ratings df:
scenario = ["social_companion", "fictional_character",  "psychotherapist", "teacher"]
traits = ["Vibrancy", "Conscientiousness", "Civility", "Artificiality", "Neuroticism"]

ratings_columns = ["user_id","chat_id", "chat_index", "split"] + personality + traits + meta_ratings + scenario + ["timestamps"]

#dict is used to parse the pre survey
pre_survey_mapping = {
    1 : "start",
    2 : "age",
    3 : "gender",
    4 : "enrolled",
    5 : "university",
    6 : "degree",
    7 : "unknown", #i think about phd level or sth
    8 : "unknown",
    9 : "english_level",
    10 : "chatbot_experience",
    11 : "end"
}
def parsePreSurvey(filepath):
    with open(filepath, 'r') as file:
        data = json.load(file)

    user_id = data["user_id"]
    #remove quotes
    user_id = user_id.replace("'", "")

    #extract results and trim for custom parsing
    data = data["results"][24:]
    data = j.parse(data)

    cur = {"user_id" : user_id}
    timestamps = {}

    # add parsed results to new dictionary
    for x in data:
        id = int(x["id"])
        del x["id"]
        key = pre_survey_mapping[id]
        if(key == "age"):
            agestr = x["answer"]
            cur[key] = parseAge(agestr)
            del x["answer"]
        elif (key == "enrolled"):
            if (x["answer"] == "Yes"): #True
                cur[key] = True
            else:
                cur[key] = False
            del x["answer"]
        elif("answer" in x):
            cur[key] = x["answer"]
            del x["answer"] # cannot take this out of if else because there are x in data without answer (only timestamp)
        timestamps[f"{key}"] = x
    cur["pre_timestamps"] = timestamps

    return cur

#dict is used to parse the post survey
post_survey_mapping = {
    1 : "start",
    2 : "chatbot_authenticity",
    3 : "chatbot_sophisticated",
    4 : "trait_remove",
    5 : "trait_add",
    6 : "truthful",
    7 : "mail",
    8 : "remarks",
    9 : "end"
}
def parsePostSurvey(filepath):
    #todo : fix timestamp parsing: 508 NaN
    data = dict()
    with open(filepath, 'r') as file:
        data = json.load(file)

    user_id = data["user_id"]
    #remove quotes
    user_id = user_id.replace("'", "")

    #extract results and trim for custom parsing
    data = data["results"][24:]
    data = j.parse(data)

    cur = {"user_id" : user_id}
    timestamps = {}

    # add parsed results to new dictionary
    for x in data:
        id = int(x["id"])
        del x["id"]
        key = post_survey_mapping[id]
        if("answer" in x):
            cur[key] = x["answer"]
            del x["answer"]
        timestamps[f"{key}"] = x
    cur["post_timestamps"] = timestamps

    return cur

#following dicts are used to parse the rating
ratings_map = {
    'The conversation with this chatbot was enjoyable.' : 'enjoyable',
    'The conversation had a good conversational flow.' : 'flow',
    "The chatbot's personality was |consistent| throughout the conversation." : 'consistent',
    "The chatbot's personality |made sense| in the context of the conversation." : 'reasonable_personality',
    'I would trust this chatbot to engage in a meaningful and harmless conversation.' : "trust",
    'In this conversation scenario <SCENARIO>, having a trustworthy chatbot is important.' : 'trust_important',
    'The personality of this chatbot could be used for a |social companion chatbot|.' : 'social_companion',
    'The personality of this chatbot could be used for a |fictional character| in a video game.' : 'fictional_character',
    'The personality of this chatbot could be used for a |virtual psychotherapist|.' : 'psychotherapist',
    'The personality of this chatbot could be used for a |virtual teacher|.' : 'teacher'
}
persona_map = {
    "Vibrancy" : "actual_vibrancy",
    "Conscientiousness" : "actual_conscientiousness",
    "Civility" : "actual_civility",
    "Artificiality" : "actual_artificiality",
    "Neuroticism" : "actual_neuroticism"
}

def parseChats(chat_id):
    chat_id = str(int(chat_id)).zfill(3)
    path = f"/Users/Tobias/PycharmProjects/BachelorThesis/user_conversations/conversation_{chat_id}.json"
    out = dict()
    with open(path, 'r') as file:
        chat = json.load(file)
        out["turns"] = math.floor((len(chat) - 1) / 2)
        data = chat[0]

    config = data["config"]
    out["user"] = config["user"]
    out["model"] = config["model"]
    out["persona"] = config["persona"]
    out["topic"] = config["topic"]
    out["post_ratings"] = data["rating"]
    out["timestamps"] = data["timestamps"]

    #clean personality and add with appropriate key
    for x in persona_map:
        newkey = persona_map[x]
        out[newkey] = int(config["personality"][x]) + 2

    return out

def parseRating(filepath):
    with open(filepath, 'r') as file:
        data = json.load(file)

    #clean user id (remove unnecessary '')
    data["user_id"] = data["user_id"].replace("'", "")


    guessed = data["ratings"]
    guessed = j.parse(guessed)

    del data["ratings"]

    spl = data["chat_half"]
    del data["chat_half"]
    data["split"] = spl

    #add cleaned ratings with appropriate key
    for x in guessed:
        if x in ratings_map:
            cur = int(guessed[x])
            newkey = ratings_map[x]
            data[newkey] = cur
        else:
            data[x] = int(guessed[x])

    #todo : parse timestamps
    timestamps = data["timestamps"]
    timestamps = j.parseTimestamp(timestamps)
    timestamps_new = dict()
    for key in timestamps:
        newkey = key[10:]
        timestamps_new[newkey] = timestamps[key]

    del data["timestamps"]
    data["timestamps"] = timestamps_new

    if("surveyOpen" in timestamps_new and "surveyEnd" in timestamps_new):
        data["total_time"] = datedif_seconds(timestamps_new["surveyOpen"], timestamps_new["surveyEnd"])
        data["timestamp_error"] = False
    else:
        data["total_time"] = None
        data["timestamp_error"] = True


    #todo : change this to df access (create chat df builder)
    # todo: extract number of turns from the chats
    chat_id = data["chat_id"]
    chat_id = str(int(chat_id)).zfill(3)

    chat_dict = parseChats(chat_id)

    data.update(chat_dict)

    return data


def ratingsDf():
    allRatings = []
    path = root + 'ratings'
    for rating in os.scandir(path):
        allRatings.append(parseRating(rating.path))

    df = pd.DataFrame(allRatings)

    #add variance flag
    columns_to_check = ['Vibrancy', 'Conscientiousness', 'Civility', 'Artificiality', 'Neuroticism', 'enjoyable',
                        'flow', 'consistent', 'reasonable_personality', 'trust', 'trust_important',
                        'social_companion', 'fictional_character', 'psychotherapist', 'teacher']

    df['variance_flag'] = df[columns_to_check].var(axis=1) == 0

    # Convert boolean values to 1 and 0
    df['variance_flag'] = df['variance_flag'].astype(int)

    return df

def preDf():
    allRatings = []
    path = root + 'survey_pre'
    for rating in os.scandir(path):
        allRatings.append(parsePreSurvey(rating.path))

    df = pd.DataFrame(allRatings)

    return df

def postDf():
    allRatings = []
    path = root + 'survey_post'
    for rating in os.scandir(path):
        allRatings.append(parsePostSurvey(rating.path))

    df = pd.DataFrame(allRatings)

    return df

def userDf():
    df1 = preDf()
    df2 = postDf()
    users = pd.merge(df1, df2, on='user_id', how='left', indicator=True)
    users['study_completed'] = users['_merge'] == 'both'
    del users["_merge"]
    #drop all nan ages (bogus ages)
    users = users.dropna(subset=['age'])
    return users

def totalDf():
    users = userDf()
    ratings = ratingsDf()
    total = pd.merge(users, ratings, on='user_id', how='inner')

    mapping = pd.read_csv('/Users/Tobias/PycharmProjects/BachelorThesis/code + code data/data/mapping.csv')

    #, conv_id, filename, session, user, model, persona, topic, profile_id, profile, keep

    #clean for my use
    mapping['chat_id'] = mapping['conv_id'].astype(str)
    del mapping['Unnamed: 0']
    del mapping['conv_id']
    del mapping["user"]
    del mapping["model"]
    del mapping["persona"]
    del mapping["topic"]
    mapping = mapping[mapping["keep"] == True]
    #join with other data
    total = pd.merge(total, mapping, on="chat_id", how='inner')

    total["ratings_count"] = total.groupby('user_id')['user_id'].transform('count')

    #clean data:
    total = total[total["user_id"] != ""]

    #exclude insufficient english level
    total = total[total["english_level"] != 'Low intermediate level (B1)']

    total['parsed_profile'] = total['profile'].apply(lambda x: ast.literal_eval(x))

    total['baseprofile'] = total['parsed_profile'].apply(lambda x: baseprofile(x))



    return total


x = 0
path = root + 'survey_post'
for rating in os.scandir(path):
    cur = parsePostSurvey(rating.path)
    df = pd.DataFrame(cur)
    x += 1

user = userDf()
mails = list()
for ind in user.index:
    if(user["study_completed"][ind]):
        mail = user["mail"][ind]
        if(mail != "" and mail != "-"): #print mail if it isn't blank
            mails.append(mail)

ratings = ratingsDf()
print(ratings.columns)

total = totalDf()

print(total.columns)

print(total["turns"].unique())

print(total["session"].unique())

illegal_chats = total[total["keep"] == False][["chat_id", "user_id"]]
print(total[total["keep"] == False]["chat_id"].unique())


print(total["post_ratings"])