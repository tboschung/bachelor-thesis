import openai
import apiKey
import prompt_library as p

all_adj = {
    "1" : "enthusiastic, joyful, cheerful, social, adventurous, curious, motivated, passionate, playful, talkative, welcoming, optimistic, active, inquisitive, communicative, humorous, determined, interested, explorative, caring",
    "2" : "logical, precise, efficient, organized, informative, smart, knowledgeable, intellectual, functional, self-disciplined, concise, thorough, objective, insightful, wise, formal, useful, stable, responsible, deep",
    "3" : "offensive, rude, arrogant, respectful, polite, accepting, harsh, confrontational, humble, irritable, tolerant, patronizing, gentle, stubborn, courteous, calm, agreeable, angry, understanding, cooperative",
    "4" : "computerized, boring, emotionless, fake, robotic, annoying, humanlike, predictable, shallow, repetitive, vague, haphazard, dysfunctional, cold, confusing, creepy, simple, realistic, inhibited, old-fashioned",
    "5" : "depressed, pessimistic, negative, fearful, complaining, frustrated, agitated, lonely, upset, shy, helpless, worried, moody, confused, scatterbrained, lost, preoccupied, absentminded, pensive, careless"
}

rework_prompt = f"""Context:
Given is a unique personality profile based on five key dimensions: Vibrancy, Conscientiousness, Civility, Artificiality, and Neuroticism.
Each dimension has a set of associated adjectives:
- Vibrancy is described by the adjectives: {all_adj["1"]}
- Conscientiousness is described by the adjectives: {all_adj["2"]}
- Civility is described by the adjectives: {all_adj["3"]}
- Artificiality is described by the adjectives: {all_adj["4"]}
- Neuroticism is described by the adjectives: {all_adj["5"]}
Each dimension has a certain intensity level from -2 (lowest) to 2 (highest).
Level -2: very low, complete opposite of that trait.
Level -1: low, mostly the opposite of that trait.
Level 0: neutral, trait not perceivable.
Level 1: high, mostly according to that trait.
Level 2: extremely high, strongly according to that trait.
The current personality settings are:
- Vibrancy: <INTENSITY_1>
- Conscientiousness: <INTENSITY_2>
- Civility: <INTENSITY_3>
- Artificiality: <INTENSITY_4>
- Neuroticism: <INTENSITY_5>
Task:
You are given a history of a conversation and you need to rewrite the newest message as if a person with the aforementioned personality settings had said it. 
The language, tone, and style of the sentence closely match the aforementioned personality. You can disregard all personality traits displayed in the conversation
Make sure the content of your message still makes sense with the context given by the conversation.
Remember:
You only return the rewritten sentence without additional punctuation or any other text.
"""

def sysMessage(message):
    return {"role": "system", "content": message}

def userMessage(message):
    return {"role": "user", "content": message}

def asstMessage(message):
    return {"role": "assistant", "content": message}

openai.api_type = "azure"
openai.api_version = "2023-05-15"
openai.api_base = "https://tobias-boschung-openai.openai.azure.com/"
openai.api_key = apiKey.getAPIKey()

def rewriteMessage(message, history, intensities):
    """
    This function puts everything together :)
    :param message: message to be reworked
    :param history: WELL STRUCTURED/FORMATTED history or sliding window of the conversation prior to this message
    :param intensities: the intensities of the 5 traits (Vibrancy, Conscientiousness, Civility, Artificiality, and Neuroticism)
    :return: the rewritten message according to the personality specified by intensities
    """
    for x in range(1, 6):
        sys_prompt = rework_prompt.replace(f"[INTENSITY_{x}]", str(intensities[x - 1]))
        
    convo = history + "\n" + "rewrite the following message" + message

    entire_task = [sysMessage(sys_prompt), userMessage(convo)]
    response = openai.ChatCompletion.create(engine="gpt-4",
                                            messages=entire_task)
    return response['choices'][0]['message']['content']

# zu diesem teil: While the last message is complete, the conversation isn't finished and could have different endings.
# Zuerst waren die generierten turns eine abgeschlossene konversation und daher die zusätzlich generierte nachricht immer sehr ähnlich
# zuerst hatte ich "but the conversation isnt finished", dann wurde mir aber die letzte Nachricht nur halb fertig zurück gegeben.
# so scheint es gut zu funktionieren
history1 = [sysMessage("your task is to generate a conversation of 8 turns. While the last message is complete, the conversation isn't finished and could have different endings. Choose two actors and a topics and make the conversation engaging by choosing an emotion that dictates the conversation. Please do not share who the actors are and what the conversation is about. Only give the user the messages of the conversation.")]

response = openai.ChatCompletion.create(engine="gpt-4",
                                        messages=history1)
generated_text = response['choices'][0]['message']['content']
history1.append(asstMessage(generated_text))
history1.append(userMessage("generate one more message"))
response = openai.ChatCompletion.create(engine="gpt-4",
                                        messages=history1)
extra_message = response['choices'][0]['message']['content']
message_partition = extra_message.split(':')
print(generated_text, end = "\n\n")
print(message_partition[1], end = "\n\n")

sys_prompt = rework_prompt
convo = generated_text + "\n" + "rewrite the following message" + message_partition[1]
#Vibrancy, Conscientiousness, Civility, Artificiality, and Neuroticism.
intensities = [0, 0, 0, 0, 2]
for x in range(1, 6):
    sys_prompt = sys_prompt.replace(f"[INTENSITY_{x}]", str(intensities[x - 1]))

history2 = [sysMessage(sys_prompt), userMessage(convo)]

response = openai.ChatCompletion.create(engine="gpt-4",
                                        messages=history2)
generated_text = response['choices'][0]['message']['content']
print(f"this is the rewritten statement with the intensities: {intensities} for Vibrancy, Conscientiousness, Civility, Artificiality, and Neuroticism", end = "\n\n")
print(generated_text)

print("to debug, this is the history:")
print(history2)
