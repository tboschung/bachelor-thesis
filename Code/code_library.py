import openai
import apiKey
import pandas as pd
import tkinter as tk
from tkinter import scrolledtext, messagebox
import json
import prompt_library as prompts
import ast
import re

openai.api_type = "azure"
openai.api_version = "2023-05-15"
openai.api_base = "https://tobias-boschung-openai.openai.azure.com/"
openai.api_key = apiKey.getAPIKey()

#helper functions to build a history
def sysMessage(message):
    return {"role": "system", "content": message}

def userMessage(message):
    return {"role": "user", "content": message}

def asstMessage(message):
    return {"role": "assistant", "content": message}

def naive_prompt(intensities):
    """
    :param intensities: array of length 5 containing the intensity for each trait
    :return: an array of messages dictating the model personality according to the parameters
    """

    system_prompt = prompts.embeddings["naive"]

    # places the intensities in the prompt
    for x in range(1,6):
        system_prompt = system_prompt.replace(f"[Intensity_{x}]", str(intensities[x-1]))

    return system_prompt

#todo: merge into one maybe
#todo: implement persona option instead of ai switch
def simplified_prompt(intensities):
    """
    :param intensities: array of length 5 containing the intensity for each trait
    :return: an array of messages dictating the model personality according to the parameters
    """

    system_prompt = prompts.embeddings["simplified"]

    # places the intensities in the prompt
    for x in range(1,6):
        system_prompt = system_prompt.replace(f"[Intensity_{x}]", str(intensities[x-1]))

    return system_prompt

def bootstrap_prompt(intensities):
    """
    :param intensities: array of length 5 containing the intensity for each trait
    :return: an array of messages dictating the model personality according to the parameters
    """

    system_prompt = prompts.embeddings["bootstrap"]

    # places the intensities in the prompt
    for x in range(1,6):
        system_prompt = system_prompt.replace(f"[Intensity_{x}]", str(intensities[x-1]))

    print(f"pre gpt:  {system_prompt}")

    #get the bootstrapped prompt from chatgpt
    response = openai.ChatCompletion.create(engine="gpt-4",
                                            messages= [sysMessage(system_prompt)])
    #todo: improve messages: pass parts as sysmessage and parts as user message

    system_prompt = response['choices'][0]['message']['content']
    #todo: save generated prompts?

    print(f"post gpt {system_prompt}")

    return system_prompt

def improved_prompt(intensities):
    """
    :param intensities: array of length 5 containing the intensity for each trait
    :return: an array of messages dictating the model personality according to the parameters
    """

    system_prompt = prompts.embeddings["improved"]

    # places the intensities in the prompt
    for x in range(1,6):
        system_prompt = system_prompt.replace(f"[Intensity_{x}]", str(intensities[x-1]))


    return system_prompt

def paramConvo(freq, temp):
    """
    interact with chatgpt and pass 2 api parameters, there is an option to log the conversation
    :param freq: real number from -2 to 2
    :param temp: real number from 0 to 2
    :return:
    """
    history = []
    i = 0

    print("type the user prompt now:")
    inp = input()

    while(inp != ""):
        history.append({'role': 'system', 'content': 'You are a helpful assistant.'})
        history.append(userMessage(inp))

        response = openai.ChatCompletion.create(engine="gpt-4",
                                                frequency_penalty=freq,
                                                temperature=temp,
                                                messages=history)

        generated_text = response['choices'][0]['message']['content']
        print("response: \n" + generated_text)
        history.append(asstMessage(generated_text))

        print()
        print("type the user prompt now: (press enter to stop)")
        inp = input()

    print("thank you for chatting with me!")

    # logging functionality
    print("do you want to log")
    log = input()
    if(log=="yes"):

        print("what do you want the log name to be?")
        logname = input()

        with open(f'logs/manual_log_{logname}.txt', 'w') as f:
            for x in history:
                if (x["role"] == "user"):
                    f.write(f"[user]: \n{x['content']} \n\n")
                elif (x["role"] == "assistant"):
                    f.write(f"[assistant]: \n{x['content']} \n\n")
                elif (x["role"] == "system"):
                    f.write(f"[system]: \n {x['content']} \n\n")

    return

def displayHistory(history, system):
    """
    neatly displays a history in the console
    :param history: the history to be displayed
    :param system: True if sys messages should be displayed, False if not
    :return a string with a neatly structured chatlog
    """

    displayStr = ""
    for x in history:
        if(x["role"] == "user"):
            displayStr += f"[user]: {x['content']} \n"
        elif(x["role"] == "assistant"):
            displayStr = f"[assistant]: {x['content']} \n"
        elif (x["role"] == "system"):
            if(system):
                displayStr = f"[system]: {x['content']} \n"

    return displayStr

#chatbot class, includes the UI to initialize and the chat UI to interact with the chatbot
# logs everything with the user id
class ChatBot:

    history = []

    def __init__(self):

        #UI-call to initialize the chatbot personality
        self.personalityUI()

        # the code for the chat UI is chatgpt generated, I only understand what I needed to for my adjustments
        # basically whenever a user message is submitted the chatbot method send_message is called

        self.root = root = tk.Tk()
        self.root.title("Chatbot GUI")

        # Create and configure the chat display area
        self.chat_display = scrolledtext.ScrolledText(root, wrap=tk.WORD, width=100, height=30)
        self.chat_display.config(state=tk.DISABLED)
        self.chat_display.grid(row=0, column=0, columnspan=2, padx=10, pady=10)

        # Create and configure the entry widget for user input
        self.user_input = tk.Entry(root, width=30)
        self.user_input.grid(row=1, column=0, padx=10, pady=10)

        # Create a button to send user input
        self.send_button = tk.Button(root, text="Send", command=self.send_message)
        self.send_button.grid(row=1, column=1, padx=10, pady=10)

        # enables the user to just press enter to send
        self.user_input.bind("<Return>", lambda event: self.send_message())

        # this displays the personality configuration
        # todo : fix personality display

        self.display_message(self.personalityReminder(), "system")
        self.ratings = []

        #todo: persona thing

        if(self.persona):
            self.personaMessage()

        self.root.mainloop()
        # when the chat window is called, the chat history is automatically logged
        self.log()

        return

    def personaMessage(self):
        personaText = self.respond()
        self.display_message(f"Bot: {personaText}", "bot")

    def log(self):
        # todo : log ratings

        file_path = 'logs/logTable.json'

        try:
            with open(file_path, 'r') as file:
                data = json.load(file)
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON in {file_path}: {e}")
            data = {}
        except FileNotFoundError:
            print(f"File not found: {file_path}")
            data = {}
        except Exception as e:
            print(f"An unexpected error occurred: {e}")
            data = {}

        user_id = self.user_id
        if user_id in data:
            id = int(data[user_id])
            id = id + 1
        else:
            id = 0

        data.update({user_id: id})

        with open('logs/logTable.json', 'w') as file:
            json.dump(data, file, indent=4)

        # todo:  add metadata

        # todo: put id back

        with open(f'logs/log_{self.user_id}.txt', 'w') as f:
            f.write(f"intensity parametrization: {self.intensities} \n")
            for x in self.history:
                rate_index = 0
                if (x["role"] == "user"):
                    f.write(f"[user]: \n{x['content']} \n\n")
                elif (x["role"] == "assistant"):
                    f.write(f"[assistant]: \n{x['content']} \n")
                    f.write(f"rating: {self.ratings[rate_index]}  \n\n")
                    rate_index += 1
                elif (x["role"] == "system"):
                    f.write(f"[system]: \n {x['content']} \n\n")

        return

    def send_message(self):
        # Get user input from the entry widget
        user_message = self.user_input.get()

        # Check if the user entered a message
        if not user_message:
            messagebox.showwarning("Warning", "Please enter a message.")
            return

        # Display the user's message in the chat display area
        self.display_message(f"User: {user_message}", "user")

        # Clear the user input entry widget
        self.user_input.delete(0, tk.END)

        self.history.append(userMessage(user_message))

        bot_response = self.respond()

        # Display the bot's response in the chat display area
        self.display_message(f"Bot: {bot_response}", "bot")

        rating = self.gptRate()

        self.display_message(f"conversation rating (|input-rating|): [{rating}]", "system")

    def display_message(self, message, tag):
        """
        displays a message in the chat UI
        :param message: the message to be displayed
        :param tag: {"user", "system", "bot"} dictates the display color
        """
        # Enable the chat display area to insert text
        self.chat_display.config(state=tk.NORMAL)

        if(tag == "user"):
            color = "grey"
        elif(tag == "system"):
            color = "green"
        elif(tag == "bot"):
            color = "black"
        # Insert the message into the chat display area
        self.chat_display.insert(tk.END, f"{message}\n", color)

        # Configure the tag to set the text color
        self.chat_display.tag_configure(color, foreground=color)

        # Disable the chat display area to make it read-only
        self.chat_display.config(state=tk.DISABLED)

        # Autoscroll to the bottom of the chat display area
        self.chat_display.yview(tk.END)

        return

    def respond(self):
        """
        :param history: the chat history
        :param pres: between -2.0 and 2.0, defaults to 0
        Positive values penalize new tokens based on whether they appear in the text so far,
        increasing the model's likelihood to talk about new topics.
        :return:
        """
        # for penalties: Negative values can be used to increase the likelihood of repetition.
        # They work by directly modifying the logits (un-normalized log-probabilities) with an additive contribution
        response = openai.ChatCompletion.create(engine="gpt-4",
                                                messages=self.history)
        generated_text = response['choices'][0]['message']['content']

        self.history.append(asstMessage(generated_text))

        return generated_text

    def gptRate(self):
        """
        uses self.history as input
        :return: gpt rating of the chatbot personality
        """

        # pass the task description as a system prompt
        rate_system = sysMessage(prompts.rating_prompt)

        # examples = userMessage(example_conversation + rating)

        # pass the conversation to be rated
        rate_user = userMessage(f"This is the conversation to rate: \n{displayHistory(self.history, False)}")

        get_rating = [rate_system, rate_user]

        response = openai.ChatCompletion.create(engine="gpt-4", temperature=0 , messages=get_rating)
        rating = response['choices'][0]['message']['content']

        #parse the rating:
        # Use ast.literal_eval to safely parse the string as a literal expression
        parsed_dict = ast.literal_eval(rating)

        # Extract values from the dictionary and convert them to a list
        result_list = list(parsed_dict.values())

        final_rate = f"vibrancy: {result_list[0]} ({abs(self.intensities[0]-result_list[0])}), 'conscientousness': {result_list[1]} ({abs(self.intensities[1]-result_list[1])}), 'decency': {result_list[2]} ({abs(self.intensities[2]-result_list[2])}), 'artificiality': {result_list[3]} ({abs(self.intensities[3]-result_list[3])}), 'neuroticism': {result_list[4]} ({abs(self.intensities[4]-result_list[4])})"

        self.ratings.append(final_rate)

        return final_rate

    def personalityUI(self):

        window = tk.Tk()
        window.title("Let's create the custom personality!")

        # define elements: user_id, intensity for each dimension, prompt type

        user_id_text = tk.Label(window, text='First type your user_id')
        user_id = tk.Entry(window)

        #get the intensity of each dimension with a slider (tkinter scale)
        trait_text = tk.Label(window, text='Build the desired personality by controlling the intensity of each trait with the sliders')

        # for each dimension we have a  text field, an intensity slider and a variable that holds the slider value
        # default value is set to 3 (neutral)
        vibrancy_text = tk.Label(window, text='Vibrancy')
        vibrancy_intensity = tk.IntVar()
        vibrancy_slider = tk.Scale(window, from_=-2, to=2, orient=tk.HORIZONTAL, variable=vibrancy_intensity)
        vibrancy_slider.set(0)

        conscientiousness_text = tk.Label(window, text='Conscientiousness')
        conscientiousness_intensity = tk.IntVar()
        conscientiousness_slider = tk.Scale(window, from_=-2, to=2, orient=tk.HORIZONTAL, variable=conscientiousness_intensity)
        conscientiousness_slider.set(0)

        decency_text = tk.Label(window, text='Decency')
        decency_intensity = tk.IntVar()
        decency_slider = tk.Scale(window, from_=-2, to=2, orient=tk.HORIZONTAL, variable=decency_intensity)
        decency_slider.set(0)

        artificiality_text = tk.Label(window, text='Artificiality')
        artificiality_intensity = tk.IntVar()
        artificiality_slider = tk.Scale(window, from_=-2, to=2, orient=tk.HORIZONTAL, variable=artificiality_intensity)
        artificiality_slider.set(0)

        neuroticism_text = tk.Label(window, text='Neuroticism')
        neuroticism_intensity = tk.IntVar()
        neuroticism_slider = tk.Scale(window, from_=-2, to=2, orient=tk.HORIZONTAL, variable=neuroticism_intensity)
        neuroticism_slider.set(0)

        persona_text = tk.Label(window, text='Do you want the model to come up with a persona?')
        persona_bool = tk.BooleanVar()
        persona_yes = tk.Radiobutton(window, text='Yes', variable=persona_bool, value=True)
        persona_no = tk.Radiobutton(window, text='No', variable=persona_bool, value=False)

        # select the prompting logic
        prompt = tk.StringVar()
        prompt_text = tk.Label(window, text = "Choose the type of prompt to tune the model")
        # 1 -> naive, 2 -> generated, 3 -> engineered
        naive_check = tk.Radiobutton(window, text='Naive Prompt', variable=prompt, value="naive")
        simplified_check = tk.Radiobutton(window, text='Simplified Prompt', variable=prompt, value ="simplified")
        bootstrap_check = tk.Radiobutton(window, text='Bootstrap Prompt (may have longer initialisation time)', variable=prompt, value ="bootstrap")
        improved_check = tk.Radiobutton(window, text='Improved Prompt', variable=prompt, value ="improved")


        confirm = tk.Button(window, text='confirm', width='50', command=lambda: self.buildPersonality(window, user_id.get(),
            vibrancy_intensity.get(), conscientiousness_intensity.get(), decency_intensity.get(), artificiality_intensity.get(),
            neuroticism_intensity.get(), prompt.get(), persona_bool.get()
        ))

        # arrange everything on grid:
        # first the user specifies their ID
        user_id_text.grid(row=0)
        user_id.grid(row=1)
        # then the personality is built with the sliders
        trait_row = 2
        trait_text.grid(row=trait_row)
        # trait text and slider should be next to each other
        vibrancy_text.grid(row=trait_row + 1)
        vibrancy_slider.grid(row=trait_row + 1, column=1)
        conscientiousness_text.grid(row=trait_row + 2)
        conscientiousness_slider.grid(row=trait_row + 2, column=1)
        decency_text.grid(row=trait_row + 3)
        decency_slider.grid(row=trait_row + 3, column=1)
        artificiality_text.grid(row=trait_row + 4)
        artificiality_slider.grid(row=trait_row + 4, column=1)
        neuroticism_text.grid(row=trait_row + 5)
        neuroticism_slider.grid(row=trait_row + 5, column=1)
        persona_text.grid(row=trait_row + 6)
        persona_yes.grid(row=trait_row + 6, column=1)
        persona_no.grid(row=trait_row + 6, column=2)
        #one line with the few-shot radio button
        # select prompt logic and the api parameters
        prompt_row = trait_row + 7
        prompt_text.grid(row = prompt_row)
        naive_check.grid(row = prompt_row+1)
        simplified_check.grid(row = prompt_row+2)
        bootstrap_check.grid(row = prompt_row+3)
        improved_check.grid(row = prompt_row+4)
        # when a user is finished, they hit the confirm button
        # this calls the buildPersonality method of the chatbot class and extracts all the variables from tkinter
        # this method also breaks out of the tkinter mainloop when the personality is built
        confirm_row = prompt_row + 5
        confirm.grid(row=confirm_row)

        tk.mainloop()

        return

    def buildPersonality(self, window, user_id, vibrancy, conscientousness, decency,
                         artificiality, neuroticism, prompt, persona):
        """
        :param window: passed so it can be destroyed when the personality is built
        :param user_id: used for logging
        :param vibrancy: intensity of this trait from 1 to 5
        :param conscientousness:
        :param decency:
        :param artificiality:
        :param neuroticism:
        :param prompt: the type of prompt to be used
        :return:
        """

        intensities = [vibrancy, conscientousness, decency, artificiality, neuroticism]
        self.intensities = intensities
        self.user_id = user_id

        # 1 -> naive, 2 -> simplified, 3 -> unmapped
        #todo: fix prompt call
        history = []

        if(prompt == "naive"):
            prompt = naive_prompt(intensities)
        elif(prompt == "simplified"):
            prompt = simplified_prompt(intensities)
        elif(prompt == "bootstrap"):
            prompt = bootstrap_prompt(intensities)
        elif(prompt == "improved"):
            prompt =improved_prompt(intensities)

        self.persona = persona
        if(persona):
            prompt += prompts.createPersona
        else:
            prompt += prompts.antiAILanguageModel

        history = [sysMessage(prompt)]

        self.history = history
        # the personality is built so the personality builder UI can be closed
        window.destroy()
        return

    def personalityReminder(self):
        reminder = f"Hello {self.user_id}, you chose the chatbot personality: \n"
        reminder += f"Vibrancy: {self.intensities[0]} \n"
        reminder += f"Conscientousness: {self.intensities[1]} \n"
        reminder += f"Decency: {self.intensities[2]} \n"
        reminder += f"Artificiality {self.intensities[3]} \n"
        reminder += f"Neuroticism {self.intensities[4]}"
        return reminder





